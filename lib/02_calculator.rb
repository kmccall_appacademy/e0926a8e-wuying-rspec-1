def add(x, y)
  x + y
end

def subtract(x, y)
  x - y
end

def sum(numbers)
  sum = 0
  numbers.each { |num| sum += num }
  sum
end

def multiplies(numbers)
  product = 1
  numbers.each { |num| product *= num }
  product
end

def power(x, y)
  x ** y
end

def factorial(n)
  return 1 if n == 0
  factorial = 1
  (1..n).each { |num| factorial * num}
  factorial
end
