def echo(word)
  word
end

def shout(words)
  words.upcase
end

def repeat(word, times = 2)
  words = Array.new(times, word)
  words.join(" ")
end

def start_of_word(word, count)
  word[0...count]
end

def first_word(string)
  words = string.split(" ")
  words.first
end

LITTLE_WORDS = ["the", "and", "over"]

def titleize(title)
  words = title.split(" ")
  titleized_words = words.map.with_index do |word, i|
    if LITTLE_WORDS.include?(word) && i != 0
      word.downcase
    else
      word.capitalize
    end
  end

  titleized_words.join(" ")
end
