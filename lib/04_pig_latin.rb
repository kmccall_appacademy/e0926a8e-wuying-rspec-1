
def translate(sentence)
  words = sentence.split(" ")
  translated_word = words.map do |word|
    translate_word(word)
  end
  translated_word.join(" ")
end

def translate_word(word)
  vowels = %w(a e i o u)
  if vowels.include?(word[0])
    "#{word}ay"
  else
    count = 0
    until vowels.include?(word[count])
      count += 1
    end
    count += 1 if word[count] == "u" && word[count - 1] == "q"
    "#{word[count..-1]}#{word[0...count]}ay"
  end
end
